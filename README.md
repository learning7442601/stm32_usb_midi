# STM32_USB_Midi



## Getting started

This project contains the source code added with 'STM32 Cube MX' platform
for 'STM32 Cube IDE'

This project demonstrates how to implement USB-Midi interface to STM32 MCU

Switch to actual branch to get the code 
(for ex: git checkout release-0.1)